Curve
=====

Description
-----------

A Curve object represents an X, Y vector data pair with a unique
value of x for every y. The only assumption on the y values is
that they increase monotonically. There is no assumption about
uniform spacing.

Usage
-----

.. code-block:: python
    
    >>> from curve.Curve import Curve
    >>> import numpy as np
    
    >>> f = 0.3
    >>> time = np.linspace(0, 5, num=1000)
    >>> voltage = np.sin(2 * np.pi * f * time)
    
    >>> wave = Curve([time, voltage])
    >>> wave.at([0, 0.25/f, 0.5/f])
    array([  0.00000000e+00,   9.96098893e-01,  -7.63373248e-05])
    
    >>> wave.cross()
    array([ 0.        ,  1.66666667,  3.33333333])
    
    >>> wave.frequency().y[-1]
    0.29999999999999999