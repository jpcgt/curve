from typing import overload
from numpy.typing import NDArray
import numpy as np

from typing import Any, Literal
import numpy.typing as npt

@overload
def hilbert(x: npt.NDArray[np.float64], N: None = None, axis: int = -1) -> npt.NDArray[np.complex128]: ...

@overload
def hilbert(x: npt.NDArray[np.float64], N: int, axis: int = -1) -> npt.NDArray[np.complex128]: ...

def butter(N: int, Wn: float | list[float], btype: Literal["low", "high", "band", "bandstop"] = "low", output: Literal["ba", "sos"] = "ba") -> tuple[NDArray[np.float64], NDArray[np.float64]]: ...

def filtfilt(
    b: NDArray[np.float64], 
    a: NDArray[np.float64], 
    x: NDArray[np.float64], 
    axis: int = -1,
    padtype: Literal["odd", "even", "constant", None] = "odd",
    padlen: int | None = None, 
    method: Literal["pad", "gust"] = "pad",
    irlen: int | None = None
) -> NDArray[np.float64]: ...
