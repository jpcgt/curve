import unittest
from .. import Curve
import numpy as np
import pickle


class TestPickling(unittest.TestCase):

    def test_pickle(self):

        x = np.linspace(0, 10)
        y = np.sin(x)

        c0 = Curve(x, y)
        p = pickle.dumps(c0)

        c1 = pickle.loads(p)

        self.assertEqual(c0, c1)
