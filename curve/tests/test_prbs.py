import unittest
from curve import Curve


prbs7str = ('00000010000011000010100011110010001011001110'
            '10100111110100001110001001001101101011011110'
            '110001101001011101110011001010101111111')


class MyTestCase(unittest.TestCase):

    def test_prbs7_ok(self):

        i0 = 33  # Where to start
        n = 544  # Total number of bits
        seq = [int(prbs7str[(i0 + i) % 127]) for i in range(n)]
        errors = Curve.prbs_check(seq)

        self.assertEqual(len(errors), n - 7)
        self.assertEqual(sum(errors), 0)

    def test_prbs7_bad1(self):

        # All 0's is not valid PRBS
        seq = [0] * 12
        n = len(seq)
        errors = Curve.prbs_check(seq)

        # Always
        self.assertEqual(len(errors), n - 7)

        # When input sequence is all zeros
        self.assertEqual(sum(errors), n - 7)

    def test_prbs7_bad2(self):

        i0 = 0  # Where to start
        n = 544  # Total number of bits
        seq = [int(prbs7str[(i0 + i) % 127]) for i in range(n)]
        error_indexes = [500]
        # error_indexes = [12, 44, 111, 112, 200]
        # error_indexes = []
        for i in error_indexes:
            seq[i] = not seq[i]
        errors = Curve.prbs_check(seq)

        errors_indexes_found = [i for i in range(n - 7) if errors[i]]
        print(errors_indexes_found)

        # self.assertEqual(len(errors), n - 7)
        self.assertGreaterEqual(sum(errors), len(error_indexes))
        self.assertEqual(len(error_indexes), sum(errors))


if __name__ == '__main__':
    unittest.main()
