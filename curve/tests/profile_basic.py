
import numpy as np
import time

# print("__name__ == '{}'".format(__name__))
# print("__package__ == '{}'".format(__package__))
# if __name__ == "__main__" and __package__ is None:
#     print("Setting __package__")
#     __package__ = "curve.tests"

import sys
sys.path.append('../')
from curve import Curve


def basic_profile(n):

    print("Basic profile: Random x steps and y values. # samples: {}".format(n))

    t = time.time()
    
    dx = np.random.rand(n)
    x = np.cumsum(dx)
    y = np.random.rand(n)
    
    xmin = min(x)
    xmax = max(x)

    print("{:8.3f} Setup...".format(time.time() - t))
    t = time.time()
    
    cv = Curve([x, y])

    print("{:8.3f} cv = Curve([x, y])".format(time.time() - t))
    t = time.time()
    
    resamp = cv.resample()

    print("{:8.3f} resamp = cv.resample()".format(time.time() - t))
    t = time.time()
    
    env = cv.envelope()

    print("{:8.3f} env = cv.envelope()".format(time.time() - t))
    t = time.time()
    
    env2 = cv.envelope2()

    print("{:8.3f} env2 = cv.envelope2()".format(time.time() - t))
    t = time.time()
    
    dff = cv.diff()

    print("{:8.3f} dff = cv.diff()".format(time.time() - t))
    t = time.time()
    
    for i in range(100):
        bot = np.random.uniform(low=xmin, high=xmax)
        top = np.random.uniform(low=bot, high=xmax)
        iv = cv.interval(xmin=bot, xmax=top)

    print("{:8.3f} Interval bot/top".format(time.time() - t))
    t = time.time()
    
    for i in range(100):
        bot = np.random.uniform(low=xmin, high=xmax)
        iv = cv.interval(xmin=bot)

    print("{:8.3f} Interval bot only".format(time.time() - t))
    t = time.time()
    
#    fc = cv.frequency()
#    
#    print time.time() - t, "fc = cv.frequency()"
#    t = time.time()
    
    m = cv.average()

    print("{:8.3f} m = cv.average()".format(time.time() - t))
    t = time.time()
    
    integ = cv.integrate()

    print("{:8.3f} integ = cv.integrate()".format(time.time() - t))
    t = time.time()
    

def cross_profile(n, period=50.0):

    print("cross2() profile: Random x steps and sine y. # samples: {}".format(n))

    t = time.time()

    dx = np.random.rand(n)
    x = np.cumsum(dx)
    y = np.sin(2 * np.pi / period * x)

    xmin = min(x)
    xmax = max(x)

    print("{:8.3f} Setup...".format(time.time() - t))
    t = time.time()

    cv = Curve([x, y])

    print("{:8.3f} cv = Curve([x, y])".format(time.time() - t))
    t = time.time()

    crosses = cv.cross(edge=None)

    print("{:8.3f} crosses = cv.cross(edge=None)".format(time.time() - t))
    t = time.time()

    crrise = cv.cross(edge='rise')

    print("{:8.3f} crrise = cv.cross(edge='rise')".format(time.time() - t))
    t = time.time()

    crrise = cv.cross(edge='fall')

    print("{:8.3f} crrise = cv.cross(edge='fall')".format(time.time() - t))
    t = time.time()


def cross2_profile(n, period=50.0):

    print("cross() profile: Random x steps and sine y. # samples: {}".format(n))

    t = time.time()

    dx = np.random.rand(n)
    x = np.cumsum(dx)
    y = np.sin(2 * np.pi / period * x)

    xmin = min(x)
    xmax = max(x)

    print("{:8.3f} Setup...".format(time.time() - t))
    t = time.time()

    cv = Curve([x, y])

    print("{:8.3f} cv = Curve([x, y])".format(time.time() - t))
    t = time.time()

    crosses = cv.cross2(edge=None)

    print("{:8.3f} crosses = cv.cross(edge=None)".format(time.time() - t))
    t = time.time()

    crrise = cv.cross2(edge='rise')

    print("{:8.3f} crrise = cv.cross(edge='rise')".format(time.time() - t))
    t = time.time()

    crrise = cv.cross2(edge='fall')

    print("{:8.3f} crrise = cv.cross(edge='fall')".format(time.time() - t))
    t = time.time()


if __name__ == "__main__":
    basic_profile(100000)
    cross_profile(100000)
    cross2_profile(100000)


