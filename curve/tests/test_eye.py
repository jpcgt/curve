import unittest
import sys
from curve import Curve
import numpy as np
import time
import pickle
import os


class TestEyeBox(unittest.TestCase):

    def test_basic(self):
        """
        This case has the eye centered.
        """

        epsilon = 1e-3

        f0 = 16e9 / 2
        start = 1.5e-8
        threshold = 0
        fname = os.path.dirname(os.path.realpath(__file__)) + '/sample_data/test_eye0.pickle'

        c = Curve.from_pickle(fname)
        success, left, right, bottom, top, width, height, nsamples = \
            c.eye_box(
                start, 
                f0, 
                threshold=threshold, 
                phase_step=0.05,
                auto_dc_offset=False
            )

        self.assertTrue(success)
        self.assertTrue(left < 0.2342 + epsilon)
        self.assertTrue(left > 0.2342 - epsilon)
        self.assertTrue(right < 0.6579 + epsilon)
        self.assertTrue(right > 0.6579 - epsilon)
        self.assertTrue(bottom < -0.0638 + epsilon)
        self.assertTrue(bottom > -0.0638 - epsilon)
        self.assertTrue(top < 0.0616 + epsilon)
        self.assertTrue(top > 0.0616 - epsilon)

    def test_wrap(self):
        """
        This case has the eye wrapping past the right limit of a UI.

        """

        epsilon = 1e-3

        f0 = 16e9 / 2
        start = 1.5e-8
        threshold = 0
        fname = os.path.dirname(os.path.realpath(__file__)) + '/sample_data/test_eye1.pickle'

        c = Curve.from_pickle(fname)
        success, left, right, bottom, top, width, height, nsamples = \
            c.eye_box(
                start, 
                f0, 
                threshold=threshold, 
                phase_step=0.05,
                auto_dc_offset=False
            )

        self.assertTrue(success)
        self.assertTrue(left < 0.6456 + epsilon)
        self.assertTrue(left > 0.6456 - epsilon)
        self.assertTrue(right < 0.0488 + epsilon)
        self.assertTrue(right > 0.0488 - epsilon)
        self.assertTrue(bottom < -0.0334 + epsilon)
        self.assertTrue(bottom > -0.0334 - epsilon)
        self.assertTrue(top < 0.0325 + epsilon)
        self.assertTrue(top > 0.0325 - epsilon)

    def test_closed(self):
        """
        This eye is closed.
        """

        f0 = 16e9 / 2
        start = 1.5e-8
        threshold = 0
        fname = os.path.dirname(os.path.realpath(__file__)) + '/sample_data/test_eye2.pickle'

        c = Curve.from_pickle(fname)
        success, left, right, bottom, top, width, height, nsamples = \
            c.eye_box(start, f0, threshold=threshold, phase_step=0.05)

        self.assertFalse(success)

    def test_start(self):
        """
        Check consistent results of eye_box() across different values of start.
        """
        f0 = 16e9 / 2
        start = 1.5e-8
        threshold = 0
        fname = os.path.dirname(os.path.realpath(__file__)) + '/sample_data/test_eye0.pickle'

        c = Curve.from_pickle(fname)
        success, left, right, bottom, top, width, height, nsamples = \
            c.eye_box(start, f0, threshold=threshold, phase_step=0.1)

        width0 = right - left if right > left else right + 1 - left
        height0 = top - bottom

        n = 100
        for i in range(100):

            start_i = start + 1/f0*(i/n)
            # print(start_i * f0)

            success, left, right, bottom, top, width, height, nsamples = \
                c.eye_box(start_i, f0, threshold=threshold, phase_step=0.1)

            width = right - left if right > left else right + 1 - left
            height = top - bottom

            # Equality up to 6 decimal places
            self.assertEqual(float("{:.6f}".format(width0)), float("{:.6f}".format(width)))
            self.assertEqual(float("{:.6f}".format(height0)), float("{:.6f}".format(height)))



