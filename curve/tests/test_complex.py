import unittest
from .. import Curve, ComplexCurveError
import numpy as np


class TestComplexCurves(unittest.TestCase):

    def test_create(self):

        x = np.linspace(0, 2 * np.pi * 10, num=100)
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve(x, y)
        cmag = c.abs()

        self.assertEqual(c.y.dtype, np.dtype(np.complex128))
        self.assertEqual(cmag.y.dtype, np.dtype(np.float64))
        self.assertTrue(cmag.y.max() < 1 + 1e6)
        self.assertTrue(cmag.y.min() > 1 - 1e6)

    def test_integrate(self):
        from matplotlib.pyplot import plot

        # Sine
        # Slices of a period
        n = 10000

        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve([x, y])

        ci = c.integrate()

        self.assertTrue(ci.abs().y[-1] < 1e-6)
        self.assertTrue(np.abs(ci.real().y[-1]) < 1e-6)
        self.assertTrue(np.abs(ci.imag().y[-1]) < 1e-6)

        # c.real().plot()
        # c.imag().plot()
        # ci.real().plot()
        # ci.imag().plot()
        #
        # print('Plotted')

    def test_envelope(self):
        # Sine
        # Slices of a period
        n = 10000

        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve([x, y])

        with self.assertRaises(ComplexCurveError):
            ce = c.envelope()

        with self.assertRaises(ComplexCurveError):
            ce = c.envelope2()

    def test_diff(self):
        # Sine
        # Slices of a period
        n = 10000

        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve([x, y])

        cd = c.diff()

        # c.real().plot()
        # c.imag().plot()
        # cd.real().plot()
        # cd.imag().plot()
        #
        # print('Plotted')

    def test_average(self):
        # Sine
        # Slices of a period
        n = 10000

        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve([x, y])

        ca = c.average()

        # c.real().plot()
        # c.imag().plot()
        # ca.real().plot()
        # ca.imag().plot()
        #
        # print('Plotted')

    def test_cross(self):
        # Sine
        # Slices of a period
        n = 10000

        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve([x, y])

        with self.assertRaises(ComplexCurveError):
            c.cross()

    def test_period_duty_frequency(self):
        # Sine
        # Slices of a period
        n = 10000

        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.cos(x) + 1j * np.sin(x)

        c = Curve([x, y])

        with self.assertRaises(ComplexCurveError):
            c.period()

        with self.assertRaises(ComplexCurveError):
            c.duty()

        with self.assertRaises(ComplexCurveError):
            c.frequency()
