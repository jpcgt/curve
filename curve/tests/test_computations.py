import unittest
from .. import Curve
import numpy as np


class TestComputations(unittest.TestCase):
    
    def test_frequency(self):
        """
        Compute the frequency of a sinewave.

        :return: None
        """
        
        # Slices of a period
        n = 1000
        
        freq = 1.0
        period = 1/freq
        
        max_period_error = period / n
        
        x = np.linspace(0, 100 * period, num=int(100 * period * n))
        y = np.sin(2 * np.pi * freq * x)
        
        c = Curve([x, y])
        fc = c.frequency()
        
        for f in fc.y:
            self.assertTrue(f < 1/(period - max_period_error))
            self.assertTrue(f > 1/(period + max_period_error))
            
    def test_integrate(self):
        """
        Integrate sinewaves.

        :return:
        """

        from matplotlib.pyplot import plot

        # Sine
        # Slices of a period
        n = 10000
        
        period = 2 * np.pi

        cycles = 100

        x = np.linspace(0, cycles * 2 * np.pi, num=int(cycles * n + 1))
        y = np.sin(x)
        
        c = Curve([x, y])
        
        ci = c.integrate()
        
        # Allowed integration error
        # TODO: Make this a function of wave parameters
        int_err_max = 1e-6
        
        # Should be zero after an integer number of periods
        self.assertTrue(np.abs(ci.y[-1]) < int_err_max)
        
        # Equals 2 integrating from 0 to pi.
        self.assertTrue(abs(ci.at(np.pi) - 2) < int_err_max)
        
        c.y = c.y**2 / (2 * np.pi)
        
        ci = c.integrate()
        
        # RMS value is 0.5
        self.assertTrue(abs(ci.at(2 * np.pi) - 0.5) < int_err_max)

    def test_cross(self):
        """
        Find zero crossings. Old method.

        :return:
        """

        from matplotlib.pyplot import plot

        for i in range(1000):
            n = 50 + int(np.random.rand() * 50)
            dx = np.random.rand(n)
            x = dx.cumsum()
            period = 10.0
            y = np.sin(2 * np.pi / period * x)

            c = Curve(x, y)

            lastx = x[-1]
            half_nperiods = int(2 * lastx / period)

            cr = c.cross()
            self.assertEqual(len(cr), half_nperiods, str(cr))

    def test_cross2(self):
        """
        Find zero crossings. New method.

        :return:
        """

        for i in range(1000):
            n = 50 + int(np.random.rand() * 50)
            dx = np.random.rand(n)
            x = dx.cumsum()
            period = 10.0
            y = np.sin(2 * np.pi / period * x)

            c = Curve(x, y)

            lastx = x[-1]
            half_nperiods = int(2 * lastx / period)

            cr = c.cross2()
            self.assertEqual(len(cr), half_nperiods, str(cr))

        # Compare old and new methods.
        for i in range(1000):
            n = 50 + int(np.random.rand() * 50)
            dx = np.random.rand(n)
            x = dx.cumsum()
            period = 10.0
            y = np.sin(2 * np.pi / period * x)

            c = Curve(x, y)

            cr2 = c.cross2()
            cr = c.cross()
            self.assertEqual(cr2.shape, cr.shape)
            self.assertTrue(np.all(cr2 == cr))


if __name__ == '__main__':
    unittest.main()
