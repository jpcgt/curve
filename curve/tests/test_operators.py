import unittest
import numpy as np
from curve import Curve


class TestOperators(unittest.TestCase):

    def test_nonuniformsum(self):
        x1 = np.linspace(0, 10, num=30)
        x2 = np.linspace(0, 10, num=100)
        y1 = np.sin(x1)
        y2 = np.cos(x2)
        sum_curve = Curve(x1, y1) + Curve(x2, y2)

        self.assertEqual(100, sum_curve.x.shape[0])

    def test_multiplication(self):
        x1 = np.linspace(0, 10, num=30)
        x2 = np.linspace(0, 10, num=100)
        y1 = np.sin(x1)
        y2 = np.cos(x2*3)
        mult_curve = Curve(x1, y1) + Curve(x2, y2)

        self.assertEqual(100, mult_curve.x.shape[0])

    def test_division(self):
        x1 = np.linspace(0, 10, num=30)
        x2 = np.linspace(0, 10, num=100)
        y1 = np.sin(x1)
        y2 = np.cos(x2 * 3)
        div_curve = Curve(x1, y1) + Curve(x2, y2)

        self.assertEqual(100, div_curve.x.shape[0])

    def test_rdivision(self):
        x1 = np.linspace(0, 10, num=30)
        y1 = np.sin(x1)
        inv_curve = 1 / Curve(x1, y1)
